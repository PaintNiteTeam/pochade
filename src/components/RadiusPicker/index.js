import React, { Component } from 'react';
import PropTypes from 'prop-types';
import './Radius.css';

class RadiusPicker extends Component {
  render() {
    return (
      <div className="RadiusPickerContainer">
        <select
          className="RadiusPicker"
          onChange={this.props.onChange}>
          <option>Select Radius</option>
          <option value={1}>1 mile</option>
          <option value={2}>2 miles</option>
          <option value={3}>3 miles</option>
          <option value={4}>4 miles</option>
          <option value={5}>5 miles</option>
        </select>
      </div>
    );
  }
}

RadiusPicker.propTypes = {
  onChange: PropTypes.func,
  value: PropTypes.number,
}
export default RadiusPicker;

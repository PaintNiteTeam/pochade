import React, { Component } from 'react';
import './section.css';
import RadiusPicker from '../RadiusPicker/index'

class Section extends Component {
  render() {
    return (
      <section className="SectionContainer">
        <p>
          To get started, edit <code>src/components/App/App.js</code> and save to reload.
        </p>
        <br/>
        <RadiusPicker/>
      </section>
    );
  }
}

export default Section;

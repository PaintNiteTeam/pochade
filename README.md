#ATTENTION
This is a PUBLIC repository. Please be considerate of your fellow developers. If you would like to create a new account for privacy, please feel free to do so!

#What to do
This repo is basically create-react-app with minor changes. We 
ask that you take a look at the [issues](https://bitbucket.org/PaintNiteTeam/pochade/issues?status=new&status=open) associated with this repo
and complete as little or as many as you would like.

The only requirement is that you at least complete [Issue#1](https://bitbucket.org/PaintNiteTeam/pochade/issues/1/as-a-user-i-need-a-way-to-view-a-paint)

1. Create a new branch with your work
2. Issue a Pull Request and include a description of the work you've done, and how to run or view it.
3. That's it!

## Installation and Running
```
git clone https://theWickedWebDev@bitbucket.org/PaintNiteTeam/pochade.git
cd pochade
npm start
```

`Will be pointing at http://localhost:3000/`

## Good luck and have fun!
